// quack - Search the web without any fear via command-line and DuckDuckGo.
// Copyright (C) 2021 J Arun Mani <j.arunman@protonmail.com>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use clap::{ArgEnum, Clap};

#[derive(ArgEnum, Debug, PartialEq)]
pub enum Format {
    JSON,
    Text,
    RichText,
}

#[derive(Clap, Debug)]
#[clap(
    author,
    about,
    version,
    after_help = "Example: `quack \"how to sing?\"`\n\nThis program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>."
)]
pub struct Options {
    /// Query to search. Use quotes to specify multiword query.
    pub query: String,

    /// Format in which to the result is returned.
    /// `rich-text` pretty formats the results.
    #[clap(short, long, arg_enum, default_value = "rich-text")]
    pub format: Format,
}
